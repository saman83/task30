import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  onLoginAttempted(message: string): void {
    alert('Kahbib says ' + message)
  }

  onRegisterAttepted(message: string): void {
    alert('Dewald always says: ' + message)
  }
}
