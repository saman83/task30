import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() message: string;

  @Output() loginAttempted: EventEmitter<string> = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  onLoginClicked() {
    this.loginAttempted.emit('Eyyyyyyyy, Sun glasses! I can eat you in a street fight...')
  }
 
}
